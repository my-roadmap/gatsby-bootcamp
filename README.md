<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
  <a href="">
    <img alt="Contentful" src="https://avatars0.githubusercontent.com/u/472182?s=200&v=4" width="60">
  </a>
  <a href="">
    <img alt="Netlify" src="https://www.netlify.com/img/press/logos/logomark.svg" width="60">
  </a>
</p>
<h1 align="center">
  Using Gatsby with Contentful and Netflify
</h1>

This is a small sample app created in the Bootcamp [The Great Gatsby Bootcamp](https://www.youtube.com/watch?v=8t0vNu2fCCM]). It shows how to simply create and save content in Contentful and loading that content in your own Gatsby app, which in term is hosted on Netlify.

## See live demo

The result of this respository can been found [here](https://unruffled-feynman-6fec81.netlify.com/contact).

## Requirements

- Node and NPM
- [Gatsby.js](https://www.gatsbyjs.org/) (globally installed)
- [Contenful account](https://www.contentful.com) access token and space id. (E.g. free tier)
- [Netlify account](https://www.netlify.com/). (E.g. free tier)

## Run locally

To run this example app locally, you must an account with contentful.

- Install NPM packages with `npm install`
- In the root directory of the repo create a `.env` file
- In that `.env` file create the variables for `CONTENTFUL_SPACE_ID` and `CONTENTFUL_ACCESS_TOKEN` and give them the corresponding values from your contentful account.
- Make sure your data model in contentful has the following fields:
  - Title [Short text]
  - Slug [Short text]
  - Published date [Date & Time]
  - Body [Rich text]
- Create one or two posts and publish them on contentful.
- Optional: Add a dummy asset. See further below under `Issues and how to fix them`.

After following the above instructions, you only need to run the app in development mode:

```
npm run develop
```

You can see the result on port 8000.

## Issues and how to fix them:

This adresses the issues that occured during the bootcamp.

- Problem: `Unable to locate env file at default location`.

  Solution: Change the 'develop' command to:

        "env-cmd -f .env.development gatsby develop"

- Problem: Images in blog posts are not displayed.

  Solution: Remove the `.cache` folder in your workspace and restart the server.

- Problem: `Unknown type "ContentfulFixed". Did you mean "ContentfulBlogPost", "ContentfulContentType", or "ContentfulBlogPostEdge"?`

  Solution: Add a dummy media element in your contentful space. For this, go into your Contentful space, click on the `Media` tab and add a single asset as a dummy image to act as a placeholder. Then restart dev server by running npm run develop or gatsby develop. [Source](https://github.com/gatsbyjs/gatsby/issues/16455#issuecomment-520720499)

## Deployments

Usually the site on Netflify will deploy your site automatically if you push on your master branch. On the other hand, if you only create a new blog post or modify an old one on Contentful, this will not trigger a redeploy and you have to log in into Netlify to manually trigger a deploy. Maybe one can use hooks to archive total automatition.
