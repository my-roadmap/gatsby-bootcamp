import React from "react"

import Layout from "../components/layout"
import Head from "../components/head"

const ContactPage = () => {
  return (
    <Layout>
      <Head title="Contact" />
      <h1>Contact</h1>
      <p>Contact me at mirkolauff [at] gmail [dot] com.</p>
      <p>
        Visit my{" "}
        <a
          href="https://gitlab.com/mirkolauff/"
          target="_blank"
          rel="noopener noreferrer"
        >
          gitlab page
        </a>
      </p>
    </Layout>
  )
}

export default ContactPage
